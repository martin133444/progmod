/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.adminPanel;
import model.rendeles;
import service.adminPanelService;

/**
 *
 * @author ASUS
 */
public class adminPanelController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            adminPanelService service = new adminPanelService();
            //Betölti az összes olyan rendelést, ami elkészítésre vár.
            if(request.getParameter("task") != null && request.getParameter("task").equals("loadOrders")){
                List<adminPanel> rendelesek = service.osszesRendeles();
                out.write(service.rendelesekToJSON(rendelesek).toString());
            }
            //A task során megjelenítjük a kiválasztott rendelés adatait.
            if(request.getParameter("task") != null && request.getParameter("task").equals("getOrderData") && request.getParameter("guid") != null){
                adminPanel r = service.getOrderData(request.getParameter("guid"));
                out.write(service.orderDataToJSON(r).toString());
            }
            //Ha az étterem elkészítette a rendelést, akkor a KÉSZ gombra kattintva az aktiv értékét átállítja 0-ra.
            if(request.getParameter("task") != null && request.getParameter("task").equals("setToInactive") && request.getParameter("guid") != null){
                adminPanel r = service.setToInactive(request.getParameter("guid"));
                out.write(service.orderDataToJSON(r).toString());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
