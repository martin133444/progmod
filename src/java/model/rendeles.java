package model;

public class rendeles {
    
    private String etel;
    private String nev;
    private String telefon;
    private String email;
    private String telepules;
    private String utca;
    private String hazszam;
    private String datum;
    private String osszeg;
    private String aktiv;
    private String guid;
    
    public rendeles(String etel, String nev, String telefon, String email, String telepules, String utca, String hazszam, String datum, String osszeg, String aktiv, String guid){
        this.etel = etel;
        this.nev = nev;
        this.telefon = telefon;
        this.email = email;
        this.telepules = telepules;
        this.utca = utca;
        this.hazszam = hazszam;
        this.datum = datum;
        this.osszeg = osszeg;
        this.aktiv = aktiv;
        this.guid = guid;
    }

    public String getEtel() {
        return etel;
    }

    public String getNev() {
        return nev;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getEmail() {
        return email;
    }

    public String getTelepules() {
        return telepules;
    }

    public String getUtca() {
        return utca;
    }

    public String getHazszam() {
        return hazszam;
    }

    public String getDatum() {
        return datum;
    }

    public String getOsszeg() {
        return osszeg;
    }
    
    public String getAktiv(){
        return aktiv;
    }
    
    public String getGuid(){
        return guid;
    }
}
