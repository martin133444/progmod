package service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import model.adminPanel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class adminPanelService {
    //A fájl, amiben a rendelések szerepelnek.
    private static File logFile = new File("C:\\Users\\Felhasználó\\Desktop\\progmod-master\\rendelesek.xml");
    //Az összes olyan rendelés bekérjük, aminek az aktiv értéke 1, tehát elkészítésre vár.
    public List<adminPanel> osszesRendeles(){
        List<adminPanel> rendelesek = new ArrayList<>();
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList rendelesNodes = xml.getElementsByTagName("Rendeles");
            for(Integer i = 0; i < rendelesNodes.getLength(); i++){
                Element rendelesElement = (Element)rendelesNodes.item(i);
                String aktiv = rendelesElement.getElementsByTagName("aktiv").item(0).getTextContent();
                if(aktiv.equals("1")){
                    String etel = rendelesElement.getElementsByTagName("etel").item(0).getTextContent().replace("+", " ");
                    String e = etel.replace("-", " ");
                    String etelD = e.replaceAll("\\d", "-");
                    String etelDone = etelD.replaceFirst("-", "");
                    String nev = rendelesElement.getElementsByTagName("nev").item(0).getTextContent();
                    String telefon = rendelesElement.getElementsByTagName("telefon").item(0).getTextContent();
                    String email = rendelesElement.getElementsByTagName("email").item(0).getTextContent();
                    String telepules = rendelesElement.getElementsByTagName("telepules").item(0).getTextContent();
                    String utca = rendelesElement.getElementsByTagName("utca").item(0).getTextContent();
                    String hazszam = rendelesElement.getElementsByTagName("hazszam").item(0).getTextContent();    
                    String datum = rendelesElement.getElementsByTagName("datum").item(0).getTextContent();
                    String osszeg = rendelesElement.getElementsByTagName("osszeg").item(0).getTextContent();
                    String guid = rendelesElement.getAttribute("guid");
                    adminPanel r = new adminPanel(etelDone, nev, telefon, email, telepules, utca, hazszam, datum, osszeg, aktiv, guid);
                    rendelesek.add(r);
                }

            }
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        return rendelesek;
    }
     
    //Rendelések JSON-ba.
    public JSONArray rendelesekToJSON(List<adminPanel> rendelesek){
       JSONArray jsons = new JSONArray();
       for(adminPanel r : rendelesek){
           JSONObject json = new JSONObject();
           json.put("etel", r.getEtel());
           json.put("nev", r.getNev());
           json.put("telefon", r.getTelefon());
           json.put("email", r.getEmail());
           json.put("telepules", r.getTelepules());
           json.put("utca", r.getUtca());
           json.put("hazszam", r.getHazszam());
           json.put("datum", r.getDatum());
           json.put("osszeg", r.getOsszeg());
           json.put("aktiv", r.getAktiv());
           json.put("guid", r.getGuid());
           jsons.put(json);
       }

       return jsons;
    }
    //A kiválasztott rendelésnek az adatait szedjük ki.
    public adminPanel getOrderData(String orderGUID) {
        adminPanel r = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList rendelesNodes = xml.getElementsByTagName("Rendeles");
            for(Integer i = 0; i < rendelesNodes.getLength(); i++){
                Element rendelesElement = (Element)rendelesNodes.item(i);
                String guid = rendelesElement.getAttribute("guid");
                if(guid.equals(orderGUID)){
                    String etel = rendelesElement.getElementsByTagName("etel").item(0).getTextContent().replace("+", " ");
                    String e = etel.replace("-", " ");
                    String etelD = e.replaceAll("\\d", "-");
                    String etelDone = etelD.replaceFirst("-", "");
                    String nev = rendelesElement.getElementsByTagName("nev").item(0).getTextContent();
                    String telefon = rendelesElement.getElementsByTagName("telefon").item(0).getTextContent();
                    String email = rendelesElement.getElementsByTagName("email").item(0).getTextContent();
                    String telepules = rendelesElement.getElementsByTagName("telepules").item(0).getTextContent();
                    String utca = rendelesElement.getElementsByTagName("utca").item(0).getTextContent();               
                    String hazszam = rendelesElement.getElementsByTagName("hazszam").item(0).getTextContent();
                    String datum = rendelesElement.getElementsByTagName("datum").item(0).getTextContent();
                    String osszeg = rendelesElement.getElementsByTagName("osszeg").item(0).getTextContent();
                    String aktiv = rendelesElement.getElementsByTagName("aktiv").item(0).getTextContent();
                    r = new adminPanel(etelDone, nev, telefon, email, telepules, utca, hazszam, datum, osszeg, aktiv, guid);
                }
            }
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        
        return r;
    }
    //Az aktív értékét átállítja 0-ra, tehát kész a rendelés.
    public adminPanel setToInactive(String orderGUID) {
        adminPanel r = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList rendelesNodes = xml.getElementsByTagName("Rendeles");
            for(Integer i = 0; i < rendelesNodes.getLength(); i++){
                Element rendelesElement = (Element)rendelesNodes.item(i);
                String guid = rendelesElement.getAttribute("guid");
                if(guid.equals(orderGUID)){
                    
                    String etel = rendelesElement.getElementsByTagName("etel").item(0).getTextContent().replace("+", " ");
                    String e = etel.replace("-", " ");
                    String etelD = e.replaceAll("\\d", "-");
                    String etelDone = etelD.replaceFirst("-", "");
                    String nev = rendelesElement.getElementsByTagName("nev").item(0).getTextContent();
                    String telefon = rendelesElement.getElementsByTagName("telefon").item(0).getTextContent();
                    String email = rendelesElement.getElementsByTagName("email").item(0).getTextContent();
                    String telepules = rendelesElement.getElementsByTagName("telepules").item(0).getTextContent();
                    String utca = rendelesElement.getElementsByTagName("utca").item(0).getTextContent();               
                    String hazszam = rendelesElement.getElementsByTagName("hazszam").item(0).getTextContent();
                    String datum = rendelesElement.getElementsByTagName("datum").item(0).getTextContent();
                    String osszeg = rendelesElement.getElementsByTagName("osszeg").item(0).getTextContent();
                    String aktiv = rendelesElement.getElementsByTagName("aktiv").item(0).getTextContent();
                    updateActive(orderGUID);
                    r = new adminPanel(etelDone, nev, telefon, email, telepules, utca, hazszam, datum, osszeg, aktiv, guid);
                }
            }
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        
        return r;
    }
    //Az aktiv mezőt frissítjük 1-ről 0-ra.
    private static void updateActive(String oGuid){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            NodeList orders = xml.getElementsByTagName("Rendeles");
            for (int i = 0; i < orders.getLength(); i++) {
                Element order = (Element)orders.item(i);
                String guid = order.getAttribute("guid");
                if(guid.equals(oGuid)){
                    if(order.getElementsByTagName("aktiv").item(0).getTextContent().equals("1")){
                        Element aktiv = (Element) order.getElementsByTagName("aktiv").item(0);
                        aktiv.setTextContent("0");
                    }
                }
            }
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            DOMSource domSource = new DOMSource(xml);
            StreamResult result = new StreamResult(logFile);
            transformer.transform(domSource, result);
            
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
    }
    
    //Kiválasztott rendelés adatai JSON-ba.
    public JSONObject orderDataToJSON(adminPanel r){
        JSONObject json = new JSONObject();
        
        json.put("etel", r.getEtel());
        json.put("nev", r.getNev());
        json.put("telefon", r.getTelefon());
        json.put("email", r.getEmail());
        json.put("telepules", r.getTelepules());
        json.put("utca", r.getUtca());
        json.put("hazszam", r.getHazszam());
        json.put("datum", r.getDatum());
        json.put("osszeg", r.getOsszeg());
        json.put("aktiv", r.getAktiv());
        json.put("guid", r.getGuid());
        
        return json;
    }
}
