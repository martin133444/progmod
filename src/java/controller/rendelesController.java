/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.rendeles;
import service.rendelesService;

/**
 *
 * @author ASUS
 */
public class rendelesController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
 
        try (PrintWriter out = response.getWriter()){
            Cookie cookie = null;
            Cookie[] cookies =  null;
            Integer osszeg = 0;
            String etelek = "";
            cookies = request.getCookies();
            if(cookies != null){
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    osszeg += Integer.parseInt(cookie.getValue());
                    etelek += URLDecoder.decode(cookie.getName(), "UTF-8") + "-";
                }
            }
          
            rendelesService service = new rendelesService();
            List<rendeles> r = new ArrayList<>();
            String etel = etelek;
            
            String nev = request.getParameter("tnev");
            byte[] nevByte = nev.getBytes(StandardCharsets.ISO_8859_1);
            nev = new String(nevByte, StandardCharsets.UTF_8);
            
            String telefon = request.getParameter("tel");
            
            String email = request.getParameter("ecim");
            byte[] emailByte = email.getBytes(StandardCharsets.ISO_8859_1);
            email = new String(emailByte, StandardCharsets.UTF_8);
            
            String telepules = request.getParameter("tp");
            byte[] telepulesByte = telepules.getBytes(StandardCharsets.ISO_8859_1);
            telepules = new String(telepulesByte, StandardCharsets.UTF_8);
            
            String utca = request.getParameter("uc");
            byte[] utcaByte = utca.getBytes(StandardCharsets.ISO_8859_1);
            utca = new String(utcaByte, StandardCharsets.UTF_8);
            
            String hazszam = request.getParameter("hszam");
            byte[] hazszamByte = hazszam.getBytes(StandardCharsets.ISO_8859_1);
            hazszam = new String(hazszamByte, StandardCharsets.UTF_8);
            
            String datum = null;
            String ossz = String.valueOf(osszeg);
            String aktiv = null;
            String guid = null;
            rendeles rend = new rendeles(etel, nev, telefon, email, telepules, utca, hazszam, datum, ossz, aktiv, guid);
            r.add(rend);
            service.writeOrderToXML(r);

        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
