package model;

public class Etelek {
    
    private String etelcsoport;
    private String nev;
    private String alapanyag;
    private String ar;
    private String id;

    public Etelek(String etelcsoport, String nev, String alapanyag, String ar, String id) {
        this.etelcsoport = etelcsoport;
        this.nev = nev;
        this.alapanyag = alapanyag;
        this.ar = ar;
        this.id = id;
    }
    
    public String getEtelcsoport(){
        return etelcsoport;
    }

    public String getNev() {
        return nev;
    }

    public String getAlapanyag() {
        return alapanyag;
    }

    public String getAr() {
        return ar;
    }
    
    public String getId(){
        return this.id;
    }

}
