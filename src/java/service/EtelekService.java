package service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import model.Etelek;


public class EtelekService {    
    
    /*Maga a fájl, amiben az ételek szerepelnek.*/
    private static File logFile = new File("C:\\Users\\Felhasználó\\Desktop\\progmod-master\\etelek.xml");
    
    /*Pizza tábla betöltése*/
    public List<Etelek> allPizza(){
        List<Etelek> pizzak = new ArrayList<>();
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList pizzaNodes = xml.getElementsByTagName("Etel");
            for(Integer i = 0; i < pizzaNodes.getLength(); i++){
                Element pizzaElement = (Element)pizzaNodes.item(i);
                String etelcsoport = pizzaElement.getElementsByTagName("etelcsoport").item(0).getTextContent();
                if(etelcsoport.equals("1")){
                    String alapanyag = pizzaElement.getElementsByTagName("alapanyag").item(0).getTextContent();
                    String ar = pizzaElement.getElementsByTagName("ar").item(0).getTextContent();
                    String nev = pizzaElement.getElementsByTagName("nev").item(0).getTextContent();
                    String id = pizzaElement.getAttribute("id");
                    Etelek p = new Etelek(etelcsoport, nev, alapanyag, ar, id);
                    pizzak.add(p);
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        return pizzak;
    }
    
    /*Hamburger tábla betöltése*/
    public List<Etelek> allHamburger(){
        List<Etelek> hamburgerek = new ArrayList<>();
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList hamburgerNodes = xml.getElementsByTagName("Etel");
            for(Integer i = 0; i < hamburgerNodes.getLength(); i++){
                Element hamburgerElement = (Element)hamburgerNodes.item(i);
                String etelcsoport = hamburgerElement.getElementsByTagName("etelcsoport").item(0).getTextContent();
                if(etelcsoport.equals("2")){
                    String alapanyag = hamburgerElement.getElementsByTagName("alapanyag").item(0).getTextContent();
                    String ar = hamburgerElement.getElementsByTagName("ar").item(0).getTextContent();
                    String nev = hamburgerElement.getElementsByTagName("nev").item(0).getTextContent();
                    String id = hamburgerElement.getAttribute("id");
                    Etelek h = new Etelek(etelcsoport, nev, alapanyag, ar, id);
                    hamburgerek.add(h);
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        return hamburgerek;
    }
    
    /*Gyros tábla betöltése*/
    public List<Etelek> allGyros(){
        List<Etelek> gyrosok = new ArrayList<>();
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList gyrosNodes = xml.getElementsByTagName("Etel");
            for(Integer i = 0; i < gyrosNodes.getLength(); i++){
                Element gyrosElement = (Element)gyrosNodes.item(i);
                String etelcsoport = gyrosElement.getElementsByTagName("etelcsoport").item(0).getTextContent();
                if(etelcsoport.equals("3")){
                    String alapanyag = gyrosElement.getElementsByTagName("alapanyag").item(0).getTextContent();
                    String ar = gyrosElement.getElementsByTagName("ar").item(0).getTextContent();
                    String nev = gyrosElement.getElementsByTagName("nev").item(0).getTextContent();
                    String id = gyrosElement.getAttribute("id");
                    Etelek g = new Etelek(etelcsoport, nev, alapanyag, ar, id);
                    gyrosok.add(g);
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        return gyrosok;
    }
    
    /*Az összes étel adatait váltjuk át JSON-ba.*/
    public JSONArray foodToJSON(List<Etelek> etelek){
        JSONArray jsons = new JSONArray();
        for(Etelek p : etelek){
            JSONObject json = new JSONObject();
            json.put("alapanyag", p.getAlapanyag());
            json.put("ar", p.getAr());
            json.put("nev", p.getNev());
            json.put("etelcsoport", p.getEtelcsoport());
            json.put("id", p.getId());
            jsons.put(json);
        }
        
        return jsons;
    }

    //Egy adott ételnek az adatait fogja mutatni, ahol foodID == id
    public Etelek getFoodData(String foodID) {
        Etelek etel = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList foodNodes = xml.getElementsByTagName("Etel");
            for(Integer i = 0; i < foodNodes.getLength(); i++){
                Element e = (Element)foodNodes.item(i);
                String id = e.getAttribute("id");
                if(id.equals(foodID)){
                    String etelcsoport = e.getElementsByTagName("etelcsoport").item(0).getTextContent();
                    String nev = e.getElementsByTagName("nev").item(0).getTextContent();
                    String alapanyag = e.getElementsByTagName("alapanyag").item(0).getTextContent();
                    String ar = e.getElementsByTagName("ar").item(0).getTextContent();
                    etel = new Etelek(etelcsoport, nev, alapanyag, ar, id);
                }
            }
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        return etel;
    }
    
    /*Az adott ételnek a dolgait dobja át JSON-ba.*/
    public JSONObject foodDataToJSON(Etelek e) {
            JSONObject json = new JSONObject();
            json.put("etelcsoport", e.getEtelcsoport());
            json.put("nev", e.getNev());
            json.put("alapanyag", e.getAlapanyag());
            json.put("ar", e.getAr());
            json.put("id", e.getId());
            return json;
    
    }
    
}