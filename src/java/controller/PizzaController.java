package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Pizza;
import org.json.JSONObject;
import service.PizzaService;

/**
 *
 * @author ASUS
 */
@WebServlet(name = "PizzaController", urlPatterns = {"/PizzaController"})
public class PizzaController extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static Integer rendelesSzamlalo = 0;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        
        try{
             PrintWriter out = response.getWriter();
             PizzaService service = new PizzaService();
             //Betölti a pizzákat az xml-ből.
             if(request.getParameter("task").equals("loadPizza")){
                List<Pizza> pizza = service.allPizza();
                out.write(service.foodToJSON(pizza).toString());
             }
             //Betölti a hamburgereket az xml-ből.
             if(request.getParameter("task").equals("loadHamburger")){
                 List<Pizza> hamburger = service.allHamburger();
                 out.write(service.foodToJSON(hamburger).toString());
             }
             //Betölti a gyrosokat az xml-ből.
             if(request.getParameter("task").equals("loadGyros")){
                 List<Pizza> gyros = service.allGyros();
                 out.write(service.foodToJSON(gyros).toString());
             }
             //A kosárra kattintás után felugrik egy ablak, hogy sikeres a rendelés, majd azt egy Cookie-ban tárolja el.
             if(request.getParameter("task")!= null && request.getParameter("task").equals("getFoodData") && 
                     request.getParameter("id") != null){
                //Kiiratja az étel adatait (neve és ára).
                Pizza e = service.getFoodData(request.getParameter("id"));
                out.write(service.foodDataToJSON(e).toString());
                //Eltároljuk egy Cookie-ban.
                String name = (++rendelesSzamlalo)+ "-" + e.getNev();
                String value = e.getAr();
                Cookie cookie = new Cookie(URLEncoder.encode(name, "UTF-8"), value);
                response.addCookie(cookie);
             }
  
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
