package service;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import model.rendeles;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class rendelesService {
    
    //A fájl, amiben a rendelések szerepelnek.
    private static File logFile = new File("C:\\Users\\Felhasználó\\Desktop\\progmod-master\\rendelesek.xml");

    //Egy új rendelés XML-be írása.
    public Boolean writeOrderToXML(List<rendeles>leadas){
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            Element root = (Element)xml.getFirstChild();
            Element rendeles = xml.createElement("Rendeles");
            Element etel = xml.createElement("etel");
            Element nev = xml.createElement("nev");
            Element telefon = xml.createElement("telefon");
            Element email = xml.createElement("email");
            Element telepules = xml.createElement("telepules");
            Element utca = xml.createElement("utca");
            Element hazszam = xml.createElement("hazszam");
            Element datum = xml.createElement("datum");
            Element osszeg = xml.createElement("osszeg");
            Element aktiv = xml.createElement("aktiv");
            for(rendeles r:leadas){
                etel.setTextContent(r.getEtel());
                nev.setTextContent(r.getNev());
                telefon.setTextContent(r.getTelefon());
                email.setTextContent(r.getEmail());
                telepules.setTextContent(r.getTelepules());
                utca.setTextContent(r.getUtca());
                hazszam.setTextContent(r.getHazszam());
                datum.setTextContent(new Date().toString());
                osszeg.setTextContent(r.getOsszeg());
                aktiv.setTextContent("1");
            }
            rendeles.appendChild(etel);
            rendeles.appendChild(nev);
            rendeles.appendChild(telefon);
            rendeles.appendChild(email);
            rendeles.appendChild(telepules);
            rendeles.appendChild(utca);
            rendeles.appendChild(hazszam);
            rendeles.appendChild(datum);
            rendeles.appendChild(osszeg);
            rendeles.appendChild(aktiv);
            rendeles.setAttribute("guid", UUID.randomUUID().toString());
            root.appendChild(rendeles);
            
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            DOMSource domSource = new DOMSource(xml);
            StreamResult result = new StreamResult(logFile);
            transformer.transform(domSource, result);
            return Boolean.TRUE;
            
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        return Boolean.FALSE;
    }
}
